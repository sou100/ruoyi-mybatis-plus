package com.ruoyi.common.enums;

import java.util.HashMap;
import java.util.Map;

import org.springframework.lang.Nullable;

/**
 * 请求方式
 *
 * @author ruoyi
 */
public enum HttpMethod {
    // HTTP请求：GET
    GET,
    // HTTP请求：HEAD
    HEAD,
    // HTTP请求：POST
    POST,
    // HTTP请求：PUT
    PUT,
    // HTTP请求：PATCH
    PATCH,
    // HTTP请求：DELETE
    DELETE,
    // HTTP请求：OPTIONS
    OPTIONS,
    // HTTP请求：TRACE
    TRACE;

    private static final Map<String, HttpMethod> MAPPINGS = new HashMap<>(16);

    static {
        for (HttpMethod httpMethod : values()) {
            MAPPINGS.put(httpMethod.name(), httpMethod);
        }
    }

    @Nullable
    public static HttpMethod resolve(@Nullable String method) {
        return (method != null ? MAPPINGS.get(method) : null);
    }

    public boolean matches(String method) {
        return (this == resolve(method));
    }
}
