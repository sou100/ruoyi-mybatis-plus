package com.ruoyi.web.controller.system;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.Tree;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.domain.tree.TreeNode;
import com.ruoyi.system.domain.SysTreeDictData;
import com.ruoyi.system.service.ISysTreeDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * 树形字典数据Controller
 *
 * @author fansd
 * @date 2021-05-31
 */
@RestController
@RequestMapping("system/tree/dict/data")
public class SysTreeDictDataController extends BaseController {
    @Autowired
    private ISysTreeDictDataService sysTreeDictDataService;

    /**
     * 分页查询树形字典数据列表
     */
    @PreAuthorize("@ss.hasPermi('system:tree:dict:list')")
    @GetMapping("list")
    public AjaxResult list(SysTreeDictData sysTreeDictData) {
        List<Object> list = sysTreeDictDataService.buildTree(sysTreeDictData);
        return AjaxResult.success(list);
    }

    /**
     * fsd 查询树形字典数据列表
     */
    @PreAuthorize("@ss.hasPermi('system:tree:dict:list')")
    @GetMapping("/tree")
    public AjaxResult tree(SysTreeDictData sysTreeDictData) {
        List<Object> list = sysTreeDictDataService.buildTree(sysTreeDictData);
        return AjaxResult.success(list);
    }

    /**
     * fsd 新增树形字典数据
     */
    @PreAuthorize("@ss.hasPermi('system:tree:dict:add')")
    @Log(title = "树形字典数据", businessType = BusinessType.INSERT)
    @PostMapping
    @Tree(treeCategory = Constants.TREE_DICT_KEY)
    public AjaxResult add(@RequestBody SysTreeDictData sysTreeDictData) {
        return toAjax(sysTreeDictDataService.insert(sysTreeDictData));
    }

    /**
     * 修改树形字典数据
     */
    @PreAuthorize("@ss.hasPermi('system:tree:dict:edit')")
    @Log(title = "树形字典数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysTreeDictData sysTreeDictData) {
        return toAjax(sysTreeDictDataService.update(sysTreeDictData));
    }

    /**
     * 删除树形字典数据
     */
    @PreAuthorize("@ss.hasPermi('system:tree:dict:remove')")
    @Log(title = "树形字典数据", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
//        List<String> idList = Arrays.asList(ids);
        return toAjax(sysTreeDictDataService.deleteByIds(ids));
    }

    /**
     * fsd 自动生成编码、层次码、层次、排序
     */
    @PostMapping(value = "/gen/autoInfo")
    public AjaxResult genAutoInfo(@RequestBody SysTreeDictData sysTreeDictData) {
        return AjaxResult.success(sysTreeDictDataService.genAutoInfo(sysTreeDictData));
    }

    /**
     * fsd 检查名称唯一性
     */
    @PostMapping(value = "/check/unique/label")
    public AjaxResult checkUniqueByLabel(@RequestBody SysTreeDictData sysTreeDictData) {
        return sysTreeDictDataService.checkUniqueByLabel(sysTreeDictData);
    }

    /**
     * fsd 校验编码
     */
    @PostMapping(value = "/check/code")
    public AjaxResult checkCode(@RequestBody SysTreeDictData sysTreeDictData) {
        return sysTreeDictDataService.checkCode(sysTreeDictData);
    }
}
