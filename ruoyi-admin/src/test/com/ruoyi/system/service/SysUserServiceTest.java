package com.ruoyi.system.service;


import com.ruoyi.common.core.domain.entity.SysUser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class SysUserServiceTest {


    @Autowired
    private ISysUserService userService;

    @Test
    public void testUserUniquer() {

        // 账号没有变化的情况， 预期返回false，不需要对name的input框进行校验
        SysUser falseEntityOne = new SysUser();
        falseEntityOne.setUserId("1");
        falseEntityOne.setUserName("admin");
        boolean falseResultOne = userService.checkUserUnique(falseEntityOne);

        // 账号有变化的情况，但是没有被其他用户使用， 预期返回false，不需要对name的input框进行校验
        SysUser falseEntityTwo = new SysUser();
        falseEntityTwo.setUserId("1");
        falseEntityTwo.setUserName("admin1");
        boolean falseResultTwo = userService.checkUserUnique(falseEntityTwo);

        // 账号有变化的情况，但是被其他用户使用， 预期返回true，需要对name的input框进行校验
        SysUser trueEntityOne = new SysUser();
        trueEntityOne.setUserId("1");
        trueEntityOne.setUserName("ry");
        boolean trueResultOne = userService.checkUserUnique(trueEntityOne);

        // 预计返回值，如果和预期结果不一致，运行Test方法会报错，反之正常log输出，不会有操作结果输出。
        Assert.assertEquals(false, falseResultOne);
        Assert.assertEquals(false, falseResultTwo);
        Assert.assertEquals(true, trueResultOne);
    }
}
