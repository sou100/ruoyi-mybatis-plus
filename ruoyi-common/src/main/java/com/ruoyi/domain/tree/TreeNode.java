package com.ruoyi.domain.tree;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.*;

/**
 * Tree基类
 *
 * @author ruoyi
 */
@Data
public class TreeNode implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 名称
     */
    private String label;

    /**
     * 父节点ID
     */
    private String pid;

    /**
     * 父节点名称
     */
    private String pLabel;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 层次码
     * 说明：
     * 顶级层次码为0
     * 非顶级使用4位数字显示，起始位1000
     */
    private String levelCode;

    /**
     * 层次
     * 说明：
     * 结合层次码，计算树形结构的的层次
     */
    private Integer levelDepth;

    /**
     * 是否叶子节点
     * <p>
     * 说明：
     * 新建节点，默认为末级节点，并更新父节点为非末级节点
     */
    private String isLeaf;

    /**
     * 子节点
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<TreeNode> children = new ArrayList<>();
}
