> 管理系统
## 开发

```bash
# 克隆项目
git clone https://gitee.com/y_project/RuoYi-Vue

# 进入项目目录
cd ruoyi-ui

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装依赖，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 http://localhost:80

## 发布

```bash
# 构建测试环境
npm run build:stage

# 构建生产环境
npm run build:prod
```

## 系统名称
```
  # title名称
    src/setting.js配置 
    title: '管理系统'
    
  # title 图标
    public\index.html中配置
    <link rel="icon" href="<%= BASE_URL %>favicon1.ico">
    <title><%= webpackConfig.name %></title>  
    
  # 系统左侧log
    src/assets/log/logo.png
    
  # 系统左侧名称
    src/consts/appConsts.js中配置
    APP_NAME = ***
    
  # 系统右侧用户管理图标
    src/store/modules/user.js配置为
    const avatar = user.avatar === '' ? require('@/assets/images/profile.png') : process.env.VUE_APP_BASE_API + user.avatar
  
```