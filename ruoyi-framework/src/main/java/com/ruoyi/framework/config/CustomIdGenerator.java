package com.ruoyi.framework.config;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.baomidou.mybatisplus.core.toolkit.Sequence;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 自定义ID生成器
 *
 * @author ruoyi
 */
@Slf4j
@Component
public class CustomIdGenerator implements IdentifierGenerator {
    private Sequence sequence = new Sequence();
    ;

    @Override
    public Number nextId(Object entity) {
        long id = sequence.nextId();
        return id;
    }

    @Override
    public String nextUUID(Object entity) {
        return null;
    }
}
